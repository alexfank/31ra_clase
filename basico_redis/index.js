const redis = require('redis');

const client = redis.createClient({
    host: 'localhost',
    port: 6379
});

client.on('error',function(err){
    console.log(err);
});

client.on('connect', function(){
    console.log('Conectado a Redis');
});

//TIPOS DE DATOS EN REDIS
//STRING
//HASHES (ALMACENADO DE OBJETOS)
//LIST (ALMACENADO LISTA DE UN ITEM)
//SETS (SIMILARES A LAS LISTAS PERO NO GUARDA REPETIDOS)

//OPERACIONES EN REDIS

//exist()
//del()
//set
//expire
//hmset
//hgetall
//get
//lrange
//sadd
//smembers

//Ejemplo de tipo de datos

//KEY, VALUE
client.set('framework', 'ReactJS');
client.set('database', 'MongoDB');
client.set('framework', 'ReactJS');
client.set('teacher', 'SteveJobs');

//Consultar los valores usando get
client.get('framework', (err, data) => {
    console.log('framework: ' + data);
});

client.get('teacher', (err,data) => {
    console.log(data);
});

client.hmset('tecnologias_hash','cache','Redis','nonrelational','MongoDB','relationaldb','MySQL');

//Usamos hgetall() para devolver todos los valores almacenados en el hash
//Redis hashes are maps

client.hgetall('tecnologias_hash',(err,datatec)=>{
    console.log(datatec);
});

//Para sacar la información de un objeto

client.hmget('tecnologias_hash','cache',(err,data)=>{
    console.log(data);
});

//Nueva version usando hset

client.hset('hash key','hashtest 1','some value',redis.print);
client.hset('hash key','hashtest 2','some other value',redis.print);

//Se puede hacer lo mismo para almacenar objetos

client.hmset('tecnologias_front',{
    'javascript': 'ReactJS',
    'css': 'Bootstrap',
    'node': 'Express'
});


// LIST

//Permite almacenar una lista de items y devuelve en formato de array

client.rpush('framework_list','ReactJS','Angular','Ember','Vue','Laravel','.NET CORE');

//Para visualizar los valores se usa el método lrange

client.lrange('framework_list',0,-1,(err,list)=>{
    console.log(list);
});

// client.del('framework_list',(err,data) =>{
//     console.log(data);
// });


// SETS

//Igual que las listas pero no permite valores duplicados

client.sadd('menu_restaurante',['Botella de Agua','Ensalada','Papitas','Pizza','Pizza','Hamburguesa','Ensalada'],(err,menudata)=>{
    console.log(menudata);
});

//Para obtener los valores del set se usa el método smembers

client.smembers('menu_restaurante',(err,menu)=>{
    console.log({
        'menu':menu
    });
    console.log(menu);
});


// VALIDANDO LLAVES

//exist
client.exists('framework',(err,data) =>{
    if(data === 1){
        console.log('Existe la llave framework');
    }else{
        console.log('No existe la llave');
    }
});

//delete
client.del('framework',(err,data)=>{
    console.log(data); //Si devuelve 1 es porque lo elimino
});

//exist (Para verificar el delete)
client.exists('framework',(err,data) =>{
    if(data === 1){
        console.log('Existe la llave framework');
    }else{
        console.log('No existe la llave');
    }
});

//Incrementar el valor usando incr
client.set('dias_vacaciones',20,()=>{
    client.incr('dias_vacaciones',(err,data)=>{
        console.log('Días de vacaciones aumentado a ',data);
    });
});

//PARA ELIMINAR LA BASE DE DATOS SERÍA DESDE redis-cli EN EL TERMINAL CON EL COMANDO FLUSHALL O FLUSHDB
