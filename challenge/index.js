const express = require('express');
const app = express();
const redis = require('redis');

const PORT = process.env.PORT || 3000;
const REDIS_PORT = process.env.PORT || 6379;

const client = redis.createClient(REDIS_PORT);
client.on("error", function(error){
    console.log(error);
});

client.set("alex", "fank");

client.get("alex", (error, rep)=>{
    if(error){
        return;
    }
    if(rep){
        console.log(rep);
    }
});

app.listen(3000, ()=>{
    console.log(`Servidor iniciado en el puerto ${PORT}`);
});